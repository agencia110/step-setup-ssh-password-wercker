#check exist password
if [[ "$WERCKER_SETUP_SSH_PASSWORD_110_PASSWORD" == "" ]] ; then
  error "password is an empty string, please check"
  exit 1
fi

apt-get update
apt-get install -y sshpass

export WERCKER_SSH="sshpass -p \"$WERCKER_SETUP_SSH_PASSWORD_110_PASSWORD\" ssh $WERCKER_SETUP_SSH_PASSWORD_110_USERNAME@$WERCKER_SETUP_SSH_PASSWORD_110_HOST -T -t -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"

echo $WERCKER_SSH

$WERCKER_SSH echo 1 &> /dev/null || failed=true

if [ "$failed" == "true" ] ; then
  error "Could not connect"
  $WERCKER_SSH -v echo 1
  exit 1
fi
  
success "Created WERCKER_SHH $WERCKER_SSH"
